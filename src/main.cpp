#include <Arduino.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include "mico.h"
#include "sans.h"


#define TFT_CS        3
#define TFT_RST        2 //-1 = connected to reset of arduino, otherwise TFT_RST=9
#define TFT_DC         1

Adafruit_ST7735 	*m_Tft;

void setup() {
    Serial.begin(9600);


    m_Tft = new Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);
    m_Tft->setSPISpeed(32000000);
    m_Tft->initR(INITR_144GREENTAB);

    m_Tft->setSPISpeed(32000000);
    m_Tft->fillScreen(ST77XX_BLACK);

    m_Tft->setSPISpeed(32000000);
}

void drawNewImage(int* img){
    int16_t x = 0;
    int16_t y = 0;
    static uint16_t color = 0;

    //m_Tft->drawPixel(x, y, color);
    m_Tft->startWrite();
    m_Tft->setAddrWindow(x, y, 128, 128);

    while(x<127 || y <128) {

        uint16_t color = img[x+128*y];
        m_Tft->SPI_WRITE16(color+color*2);

        color ++;

        x++;
        if(x>127){
            y++;
            x=0;
        }
    }


    m_Tft->endWrite();
}

void loop() {
    Serial.write("Ouiii\n");
    drawNewImage(const_cast<int *>(mico));
    delay(1000);
    drawNewImage(const_cast<int *>(sans));
    delay(1000);
}